package com.MySelf;
public class Main {

    public static void main(String[] args) {
        // Declaration
        final String name = "Aiman bin Anizan",
                hobbies = "Playing guitar",
                matricNo = "2021100647",
                programCode = "CS266",
                campus = "Jasin",
                dad = "Anizan",
                mom = "Rosliza",
                siblings = "Alif";
        final int age = 21;

        new MySelf(name, age, hobbies).PrintInfo();

        new MyStudent(name, age, hobbies, matricNo, programCode, campus).PrintInfo();

        new MyFamily(name, age, hobbies, dad, mom, siblings).PrintInfo();
    }
}
