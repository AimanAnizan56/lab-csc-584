package com.MySelf;

public class MySelf implements MyVehicle{
    private String MyName;
    private int MyAge;
    private String MyHobbies;

    public MySelf(String myName, int myAge, String myHobbies) {
        MyName = myName;
        MyAge = myAge;
        MyHobbies = myHobbies;
    }

    public String getMyName() {
        return MyName;
    }
    public void setMyName(String myName) {
        MyName = myName;
    }

    public int getMyAge() {
        return MyAge;
    }
    public void setMyAge(int myAge) {
        MyAge = myAge;
    }

    public String getMyHobbies() {
        return MyHobbies;
    }
    public void setMyHobbies(String myHobbies) {
        MyHobbies = myHobbies;
    }

    public void PrintInfo() {
        this.PrintMySelf();
        this.PrintVehicle();
    }

    protected void PrintMySelf() {
        final var $ = System.out;

        $.println();
        $.println("Name: " + this.getMyName());
        $.println("Age: " + this.getMyAge());
        $.println("Hobbies: " + this.getMyHobbies());
    }

    protected void PrintVehicle() {
        final var $ = System.out;

        $.println("Vehicle Type: " + MyVehicle.MyVehicleType);
        $.println("Vehicle Brand: " + MyVehicle.MyVehicleBrand);
        $.println("Vehicle ID: " + MyVehicle.MyVehicleID);
    }
}
