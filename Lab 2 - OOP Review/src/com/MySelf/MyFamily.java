package com.MySelf;

public class MyFamily extends MySelf{
    private String MyDad;
    private String MyMom;
    private String MySiblings;


    public MyFamily(String myName, int myAge, String myHobbies, String myDad, String myMom, String mySiblings) {
        super(myName, myAge, myHobbies);
        MyDad = myDad;
        MyMom = myMom;
        MySiblings = mySiblings;
    }

    public String getMyDad() {
        return MyDad;
    }
    public void setMyDad(String myDad) {
        MyDad = myDad;
    }

    public String getMyMom() {
        return MyMom;
    }
    public void setMyMom(String myMom) {
        MyMom = myMom;
    }

    public String getMySiblings() {
        return MySiblings;
    }
    public void setMySiblings(String mySiblings) {
        MySiblings = mySiblings;
    }

    @Override
    public void PrintInfo() {
        final var $ = System.out;

        super.PrintMySelf();
        $.println("Dad: " + this.getMyDad());
        $.println("Mom: " + this.getMyMom());
        $.println("Siblings: " + this.getMySiblings());
        super.PrintVehicle();
    }
}
