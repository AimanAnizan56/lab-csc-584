package com.MySelf;

public class MyStudent extends MySelf{
    private String MyMatricNo;
    private String MyProgramCode;
    private String MyCampus;

    public MyStudent(String myName, int myAge, String myHobbies, String myMatricNo, String myProgramCode, String myCampus) {
        super(myName, myAge, myHobbies);
        MyMatricNo = myMatricNo;
        MyProgramCode = myProgramCode;
        MyCampus = myCampus;
    }

    public String getMyMatricNo() {
        return MyMatricNo;
    }
    public void setMyMatricNo(String myMatricNo) {
        MyMatricNo = myMatricNo;
    }

    public String getMyProgramCode() {
        return MyProgramCode;
    }
    public void setMyProgramCode(String myProgramCode) {
        MyProgramCode = myProgramCode;
    }

    public String getMyCampus() {
        return MyCampus;
    }
    public void setMyCampus(String myCampus) {
        MyCampus = myCampus;
    }

    @Override
    public void PrintInfo() {
        final var $ = System.out;

        super.PrintMySelf();
        $.println("Matric No: " + this.getMyMatricNo());
        $.println("Program Code: " + this.getMyProgramCode());
        $.println("Campus: " + this.getMyCampus());
        super.PrintVehicle();
    }
}
