<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <title>JSP - Hello World</title>
</head>
<body>
    <%@include file="Menu.html"%>
    <h1><%= "Hi! My name is Aiman Anizan" %> </h1>
    <br/>
    <a href="hello-servlet">Hello Servlet</a>
</body>
</html>