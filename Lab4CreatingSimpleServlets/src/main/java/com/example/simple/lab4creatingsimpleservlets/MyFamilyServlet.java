package com.example.simple.lab4creatingsimpleservlets;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "MyFamilyServlet", value = "/MyFamilyServlet")
public class MyFamilyServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        final String name = "Aiman bin Anizan",
                hobbies = "Watching timelapse video and playing guitar",
                dad = "Anizan",
                mom = "Rosliza",
                siblings = "Alif";

        final int age = 21;

        MyFamily family = new MyFamily(name, age, hobbies, dad, mom, siblings);

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.println("<html>");
        out.println("<head>");
        out.println("<link rel='stylesheet' href='style.css'>");
        out.println("</head>");
        out.println("<body>");

        out.println("<ul class='listbox'>");
        out.println(family.PrintInfo());
        out.println("</ul>");

        out.println("</body></html>");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
