<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <title>JSP - Hello World</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
     <%@include file="Menu.html"%>
     <div class="container">
        <h1><%= "Hi! My name is <span style='color: coral'>Aiman Anizan</span>" %> </h1>
         <div class="servlet-link">
            <a href="hello-servlet">Hello Servlet</a>
            <a href="MyselfServlet">MySelf Servlet</a>
            <a href="MyFamilyServlet">MyFamily Servlet</a>
            <a href="MyStudentServlet">MyStudent Servlet</a>
         </div>
     </div>
</body>
</html>