package com.example.lab6jspessentialsandcss;

import java.io.PrintStream;

public class MySelf implements MyVehicle{
    private String MyName;
    private int MyAge;
    private String MyHobbies;

    public MySelf(String myName, int myAge, String myHobbies) {
        MyName = myName;
        MyAge = myAge;
        MyHobbies = myHobbies;
    }

    public String getMyName() {
        return MyName;
    }
    public void setMyName(String myName) {
        MyName = myName;
    }

    public int getMyAge() {
        return MyAge;
    }
    public void setMyAge(int myAge) {
        MyAge = myAge;
    }

    public String getMyHobbies() {
        return MyHobbies;
    }
    public void setMyHobbies(String myHobbies) {
        MyHobbies = myHobbies;
    }

    public String PrintInfo() {
        String $ = null;

        $  = this.PrintMySelf();
        $ += this.PrintVehicle();

        return $;
    }

    protected String PrintMySelf() {
        String $ = null;

        $ = "<li>" + "<b>Name:</b> " + this.getMyName() + "</li>";
        $ += "<li>" + "<b>Age:</b> " + this.getMyAge() + "</li>";
        $ += "<li>" + "<b>Hobbies:</b> " + this.getMyHobbies() + "</li>";

        return $;

    }

    protected String PrintVehicle() {
        String $ = null;

        $ = "<li>" + "<b>Vehicle Type:</b> " + MyVehicle.MyVehicleType + "</li>";
        $ += "<li>" + "<b>Vehicle Brand:</b> " + MyVehicle.MyVehicleBrand + "</li>";
        $ += "<li>" + "<b>Vehicle ID:</b> " + MyVehicle.MyVehicleID + "</li>";

        return $;
    }
}
