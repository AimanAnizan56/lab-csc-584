package com.example.lab6jspessentialsandcss;

import java.io.PrintStream;

public class MyFamily extends MySelf{
    private String MyDad;
    private String MyMom;
    private String MySiblings;


    public MyFamily(String myName, int myAge, String myHobbies, String myDad, String myMom, String mySiblings) {
        super(myName, myAge, myHobbies);
        MyDad = myDad;
        MyMom = myMom;
        MySiblings = mySiblings;
    }

    public String getMyDad() {
        return MyDad;
    }
    public void setMyDad(String myDad) {
        MyDad = myDad;
    }

    public String getMyMom() {
        return MyMom;
    }
    public void setMyMom(String myMom) {
        MyMom = myMom;
    }

    public String getMySiblings() {
        return MySiblings;
    }
    public void setMySiblings(String mySiblings) {
        MySiblings = mySiblings;
    }

    @Override
    public String PrintInfo() {
        String $ = "";

        $ += super.PrintMySelf();
        $ += "<li>" + "<b>Dad:</b> " + this.getMyDad() + "</li>";
        $ += "<li>" + "<b>Mom:</b> " + this.getMyMom() + "</li>";
        $ += "<li>" + "<b>Siblings:</b> " + this.getMySiblings() + "</li>";
        $ += super.PrintVehicle() + "</li>";

        return $;
    }
}
