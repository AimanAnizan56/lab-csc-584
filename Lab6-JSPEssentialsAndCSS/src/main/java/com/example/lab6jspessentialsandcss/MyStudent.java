package com.example.lab6jspessentialsandcss;

public class MyStudent extends MySelf{
    private String MyMatricNo;
    private String MyProgramCode;
    private String MyCampus;

    public MyStudent(String myName, int myAge, String myHobbies, String myMatricNo, String myProgramCode, String myCampus) {
        super(myName, myAge, myHobbies);
        MyMatricNo = myMatricNo;
        MyProgramCode = myProgramCode;
        MyCampus = myCampus;
    }

    public String getMyMatricNo() {
        return MyMatricNo;
    }
    public void setMyMatricNo(String myMatricNo) {
        MyMatricNo = myMatricNo;
    }

    public String getMyProgramCode() {
        return MyProgramCode;
    }
    public void setMyProgramCode(String myProgramCode) {
        MyProgramCode = myProgramCode;
    }

    public String getMyCampus() {
        return MyCampus;
    }
    public void setMyCampus(String myCampus) {
        MyCampus = myCampus;
    }

    @Override
    public String PrintInfo() {
        String $ = "";

        $+= super.PrintMySelf();
        $ += "<li>" + "<b>Matric No:</b> " + this.getMyMatricNo() + "</li>";
        $ += "<li>" + "<b>Program Code:</b> " + this.getMyProgramCode() + "</li>";
        $ += "<li>" + "<b>Campus:</b> " + this.getMyCampus() + "</li>";
        $ += super.PrintVehicle();

        return $;
    }
}
