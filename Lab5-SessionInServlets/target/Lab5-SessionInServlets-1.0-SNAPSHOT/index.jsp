<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <title>JSP - Hello World</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
     <%@include file="Menu.html"%>
     <div class="container">
        <h1><%= "Hi! My name is <span style='color: coral'>Aiman Anizan</span>" %> </h1>
         <div class="servlet-link">
            <a href="hello-servlet">Hello Servlet</a>
            <a href="MyselfServlet">MySelf Servlet</a>
            <a href="MyFamilyServlet">MyFamily Servlet</a>
            <a href="MyStudentServlet">MyStudent Servlet</a>
         </div>
         <form name="allform" action="index.jsp" method="post">
            <div>
                <label for="myName">Name</label>
                <input type="text" name="myName" id="myName" autocomplete="off" required>
            </div>
             <div>
                 <label for="myAge">Age</label>
                <input type="number" name="myAge" id="myAge" autocomplete="off" required>
             </div>
             <div>
                 <label for="myHobbies">Hobbies</label>
                <input type="text" name="myHobbies" id="myHobbies" autocomplete="off" required>
             </div>
             <div>
                 <label for="myDad">Dad</label>
                <input type="text" name="myDad" id="myDad" autocomplete="off" required>
             </div>
             <div>
                 <label for="myMom">Mom</label>
                <input type="text" name="myMom" id="myMom" autocomplete="off" required>
             </div>
             <div>
                 <label for="mySiblings">Siblings</label>
                <input type="text" name="mySiblings" id="mySiblings" autocomplete="off" required>
             </div>
             <div>
                 <label for="myMatricNo">Matric No</label>
                <input type="text" name="myMatricNo" id="myMatricNo" autocomplete="off" required>
             </div>
             <div>
                 <label for="myProgramCode">Program Code</label>
                <input type="text" name="myProgramCode" id="myProgramCode" autocomplete="off" required>
             </div>
             <div>
                 <label for="myCampus">Campus</label>
                <input type="text" name="myCampus" id="myCampus" autocomplete="off" required>
             </div>
             <div>
                 <button type="submit">Submit</button>
             </div>
         </form>
         <%
             String myName = request.getParameter("myName");
             String myAge = request.getParameter("myAge");
             String myHobbies = request.getParameter("myHobbies");
             String myDad = request.getParameter("myDad");
             String myMom = request.getParameter("myMom");
             String mySiblings = request.getParameter("mySiblings");
             String myMatricNo = request.getParameter("myMatricNo");
             String myProgramCode = request.getParameter("myProgramCode");
             String myCampus = request.getParameter("myCampus");

             boolean isNull =
                     myName == null ||
                     myAge == null ||
                     myHobbies == null ||
                     myDad == null ||
                     myMom == null ||
                     mySiblings == null ||
                     myMatricNo == null ||
                     myProgramCode == null ||
                     myCampus == null;

             if(!isNull) {
                 session.setAttribute("myName", myName);
                 session.setAttribute("myAge", myAge);
                 session.setAttribute("myHobbies", myHobbies);
                 session.setAttribute("myDad", myDad);
                 session.setAttribute("myMom", myMom);
                 session.setAttribute("mySiblings", mySiblings);
                 session.setAttribute("myMatricNo", myMatricNo);
                 session.setAttribute("myProgramCode", myProgramCode);
                 session.setAttribute("myCampus", myCampus);

                 out.println("<h1 class='session-message'>Session Created</h1>");
             }
         %>
     </div>
</body>
</html>